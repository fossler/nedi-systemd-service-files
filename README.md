# NeDi Systemd Service Files | www.nedi.ch

How to install the service files

1.  Clone the files to the local machine: <br/>
`git clone https://gitlab.com/fossler/nedi-systemd-service-files.git`<br/>

2.  Adapt the files to your need. Change variables User, Group and the path to the scripts:<br/>
`cd nedi-systemd-service-files`<br/>
`vim nedi-monitor.service`<br/>
`vim nedi-syslog.service`<br/>

3. Copy the files in /etc/systemd/system/:<br/>
`cp nedi-* /etc/systemd/system/`<br/>

4. Let systemd know about the new service files:<br/>
`systemctl daemon-reload`<br/>

5. Enable the services and start them:<br/>
`systemct enable --now nedi-monitor.service && systemctl status nedi-monitor.service`<br/>
`systemct enable --now nedi-syslog.service && systemctl status nedi-syslog.service`<br/>